package com.ecommerce.productapi.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Currency;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.ecommerce.productapi.dto.ProductDTO;
import com.ecommerce.productapi.exception.ProductNotFoundException;
import com.ecommerce.productapi.model.Category;
import com.ecommerce.productapi.model.Product;
import com.ecommerce.productapi.repository.ProductRepository;
import com.ecommerce.productapi.service.CurrencyService;
import com.ecommerce.productapi.service.ProductService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceImplTest {

	@Value("${api.default.currency}")
	private String defaultCurrency;
	
	@MockBean
	private ProductRepository productRepository;
	
	@MockBean
	private CurrencyService currencyService;
	
	@Autowired
	private ProductService productService;
	
	@Test
	public void whenValidProduct_thenProductShouldBeFound() {
		Product mockProduct = mockProduct(1L);
		Mockito.when(productRepository.findById(mockProduct.getId())).thenReturn(Optional.of(mockProduct));
		
		Product product = productService.getProductById(mockProduct.getId());
		
		assertNotNull(product);
		assertEquals(mockProduct, product);
	}
	
	@Test(expected = ProductNotFoundException.class)
	public void whenInvalidProduct_thenExceptionShouldBeThrown() {
		Long productId = 1L;
		Mockito.when(productRepository.findById(productId)).thenReturn(Optional.empty());
		
		productService.getProductById(productId);
	}
	
	@Test
	public void whenThreeProductsAdded_thenSameThreeProductsShouldBeReturned() {
		Product mockProduct1 = mockProduct(1L);
		Product mockProduct2 = mockProduct(2L);
		Product mockProduct3 = mockProduct(3L);
		List<Product> mockProductList = Arrays.asList(mockProduct1, mockProduct2, mockProduct3);
		
		Mockito.when(productRepository.findAll()).thenReturn(mockProductList);
		
		List<Product> products = productService.listAllProducts();
		
		assertNotNull(products);
		assertEquals(3, products.size());
		assertEquals(mockProduct1, products.get(0));
		assertEquals(mockProduct2, products.get(1));
		assertEquals(mockProduct3, products.get(2));
	}
	
	@Test
	public void whenValidProductIdWithCategoryAdded_thenCategoriesShouldBeFound() {
		Product mockProduct = mockProduct(1L);
		Mockito.when(productRepository.findById(mockProduct.getId())).thenReturn(Optional.of(mockProduct));
		
		List<Category> categories = productService.getCategoriesByProductId(mockProduct.getId());
		
		assertNotNull(categories);
		assertEquals(1, categories.size());
	}
	
	@Test(expected = ProductNotFoundException.class)
	public void whenLookingForCategoriesOfInvalidProduct_thenExceptionShouldBeThrown() {
		Long productId = 1L;
		Mockito.when(productRepository.findById(productId)).thenReturn(Optional.empty());
		
		productService.getCategoriesByProductId(productId);
	}
	
	@Test
	public void whenAddingProductWithDefaultCurrency_thenProductAddedHasNoChange() {
		Product mockProduct = mockProduct(1L);
		ProductDTO productDTO = new ProductDTO(mockProduct.getDescription(), mockProduct.getPrice(), null);
		Mockito.when(productRepository.save(any(Product.class))).thenReturn(mockProduct);
		
		Product product = productService.addProduct(productDTO);
		
		assertNotNull(product);
		assertNotNull(product.getId());
		assertEquals(productDTO.getPrice(), product.getPrice());
		verifyZeroInteractions(currencyService);
	}
	
	@Test
	public void whenAddingProductWithOtherCurrency_thenProductAddedHasPriceChange() {
		Product mockProduct = mockProduct(1L);
		ProductDTO productDTO = new ProductDTO(mockProduct.getDescription(), mockProduct.getPrice(), Currency.getInstance("USD"));
		BigDecimal convertedAmount = new BigDecimal("316.80");
		mockProduct.setPrice(convertedAmount);
		Mockito.when(productRepository.save(any(Product.class))).thenReturn(mockProduct);
		Mockito.when(currencyService.convertAmount(productDTO.getCurrency(), Currency.getInstance(defaultCurrency), productDTO.getPrice())).thenReturn(convertedAmount);
		
		Product product = productService.addProduct(productDTO);
		
		assertNotNull(product);
		assertNotNull(product.getId());
		assertEquals(convertedAmount, product.getPrice());
		verify(currencyService, times(1)).convertAmount(any(Currency.class), any(Currency.class), any(BigDecimal.class));
	}
	
	@Test(expected = ProductNotFoundException.class)
	public void whenAddingCategoriesToInvalidProduct_thenExceptionShouldBeThrown() {
		Long productId = 1L;
		Mockito.when(productRepository.findById(productId)).thenReturn(Optional.empty());
		
		productService.addCategoriesToProduct(productId, Collections.singletonList(1L));
	}
	
	@Test
	public void whenAddingCategoriesToValidProduct_thenReturnProductWithSameCategories() {
		Product mockProduct = mockProduct(1L);
		Mockito.when(productRepository.findById(mockProduct.getId())).thenReturn(Optional.of(mockProduct));
		
		Category mockCategory1 = new Category(1L, "sport");
		Category mockCategory2 = new Category(2L, "running");
		Category mockCategory3 = new Category(3L, "shoes");
		Set<Category> mockedCategories = new HashSet<>();
		mockedCategories.add(mockCategory1);
		mockedCategories.add(mockCategory2);
		mockedCategories.add(mockCategory3);
		mockProduct.setCategories(mockedCategories);
		Mockito.when(productRepository.save(any(Product.class))).thenReturn(mockProduct);
		
		Product product = productService.addCategoriesToProduct(mockProduct.getId(), Arrays.asList(1L, 2L, 3L));
		
		assertNotNull(product);
		assertEquals(mockProduct.getId(), product.getId());
		assertEquals(3, product.getCategories().size());
	}
	
	private Product mockProduct(Long productId) {
		Product mockProduct = new Product();
		mockProduct.setId(productId);
		mockProduct.setDescription("Moto G7 Plus");
		mockProduct.setPrice(new BigDecimal("279.99"));
		Category mockCategory = new Category();
		mockCategory.setId(1L);
		mockCategory.setDescription("mobile");
		mockProduct.setCategories(Collections.singleton(mockCategory));
		return mockProduct;
	}
}
