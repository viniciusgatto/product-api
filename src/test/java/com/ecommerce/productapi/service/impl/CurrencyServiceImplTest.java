package com.ecommerce.productapi.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

import java.math.BigDecimal;
import java.net.URI;
import java.util.Currency;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.ecommerce.productapi.exception.CurrencyConversionException;
import com.ecommerce.productapi.service.CurrencyService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CurrencyServiceImplTest {
	
	@MockBean
	private RestTemplate restTemplate;
	
	@Autowired
	private CurrencyService currencyService;
	
	@Test
	public void whenExternalServiceResponseIsOk_thenReturnConvertedAmount() {
		
		String mockPayload = "{" +
			"\"success\":true," +
			"\"timestamp\":1555270444," +
			"\"base\":\"EUR\"," +
			"\"date\":\"2019-04-14\","+
			"\"rates\":" + 
				" {\"USD\":1.129899}" +
			"}";
		ResponseEntity<Object> mockResponseEntity = new ResponseEntity<>(mockPayload, HttpStatus.OK);
		Mockito.when(restTemplate.getForEntity(any(URI.class), any())).thenReturn(mockResponseEntity);
		
		Currency from = Currency.getInstance("USD");
		Currency to = Currency.getInstance("EUR");
		BigDecimal amount = new BigDecimal("100");
		BigDecimal convertedAmount = currencyService.convertAmount(from, to, amount);
		
		assertNotNull(convertedAmount);
		assertEquals(new BigDecimal("88.50"), convertedAmount);
	}
	
	@Test(expected = CurrencyConversionException.class)
	public void whenExternalServiceResponseIsNotOk_thenExceptionShouldBeThrown() {
		
		ResponseEntity<Object> mockResponseEntity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		Mockito.when(restTemplate.getForEntity(any(URI.class), any())).thenReturn(mockResponseEntity);
		
		Currency from = Currency.getInstance("USD");
		Currency to = Currency.getInstance("EUR");
		BigDecimal amount = new BigDecimal("100");
		currencyService.convertAmount(from, to, amount);
	}
}