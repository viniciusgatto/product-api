package com.ecommerce.productapi.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.ecommerce.productapi.dto.CategoryDTO;
import com.ecommerce.productapi.exception.CategoryNotFoundException;
import com.ecommerce.productapi.model.Category;
import com.ecommerce.productapi.model.Product;
import com.ecommerce.productapi.repository.CategoryRepository;
import com.ecommerce.productapi.repository.ProductRepository;
import com.ecommerce.productapi.service.CategoryService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryServiceImplTest {

	@MockBean
	private CategoryRepository categoryRepository;
	
	@MockBean
	private ProductRepository productRepository;
	
	@Autowired
	private CategoryService categoryService;
	
	@Test
	public void whenValidCategory_thenCategoryShouldBeFound() {
		Category mockCategory = new Category(1L, "shirt");
		Mockito.when(categoryRepository.findById(mockCategory.getId())).thenReturn(Optional.of(mockCategory));
		
		Category category = categoryService.getCategoryById(mockCategory.getId());
		
		assertNotNull(category);
		assertEquals(mockCategory, category);
	}
	
	@Test(expected = CategoryNotFoundException.class)
	public void whenInvalidCategory_thenExceptionShouldBeThrown() {
		Long categoryId = 1L;
		Mockito.when(categoryRepository.findById(categoryId)).thenReturn(Optional.empty());
		
		categoryService.getCategoryById(categoryId);
	}
	
	@Test
	public void whenThreeCategoriesAdded_thenSameThreeCategoriesShouldBeReturned() {
		Category mockCategory1 = new Category(1L, "sport");
		Category mockCategory2 = new Category(2L, "running");
		Category mockCategory3 = new Category(3L, "shoes");
		List<Category> mockCategoryList = Arrays.asList(mockCategory1, mockCategory2, mockCategory3);
		
		Mockito.when(categoryRepository.findAll()).thenReturn(mockCategoryList);
		
		List<Category> categories = categoryService.listAllCategories();
		
		assertNotNull(categories);
		assertEquals(3, categories.size());
		assertEquals(mockCategory1, categories.get(0));
		assertEquals(mockCategory2, categories.get(1));
		assertEquals(mockCategory3, categories.get(2));
	}
	
	@Test
	public void whenAddingCategoryWithNewDescription_thenCategoryShouldBeAdded() {
		Category mockCategory = new Category(1L, "sport");
		CategoryDTO categoryDTO = new CategoryDTO();
		categoryDTO.setDescription("shirt");
		Mockito.when(categoryRepository.save(any(Category.class))).thenReturn(mockCategory);
		
		Category category = categoryService.addCategory(categoryDTO);
		
		assertNotNull(category);
		assertEquals(mockCategory, category);
	}
	
	@Test
	public void whenValidCategory_thenProductsShouldBeFound() {
		Long categoryId = 1L;
		List<Product> mockProductList = Arrays.asList(mockProduct(1L), mockProduct(2L), mockProduct(3L), mockProduct(4L));
		Mockito.when(productRepository.getProductsByCategoryId(categoryId)).thenReturn(mockProductList);
		
		List<Product> products = categoryService.getProductsByCategoryId(categoryId);
		
		assertNotNull(products);
		assertEquals(4, products.size());
	}
	
	private Product mockProduct(Long productId) {
		Product mockProduct = new Product();
		mockProduct.setId(productId);
		mockProduct.setDescription("Moto G7 Plus");
		mockProduct.setPrice(new BigDecimal("279.99"));
		return mockProduct;
	}
}
