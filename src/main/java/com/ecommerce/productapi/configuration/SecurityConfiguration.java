package com.ecommerce.productapi.configuration;

import java.util.Collections;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	public static final String REALM_NAME = "MyRealm";
	public static final String API_KEY_PARAM = "apikey";
	public static final Pattern AUTHORIZATION_HEADER_PATTERN = Pattern.compile(String.format("%s %s=\"(\\S+)\"", REALM_NAME, API_KEY_PARAM));

	@Autowired
	private AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> authorizationUserDetailsService;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.addFilterAfter(preAuthenticationFilter(), RequestHeaderAuthenticationFilter.class)
		.authorizeRequests()
		.antMatchers("/categories/**").authenticated()
		.antMatchers("/products/**").authenticated()
		.anyRequest().permitAll()
		.and()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and()
		.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint())
		.and()
		.csrf().disable();
	}

	@Bean
	public RequestHeaderAuthenticationFilter preAuthenticationFilter() {
		RequestHeaderAuthenticationFilter preAuthenticationFilter = new RequestHeaderAuthenticationFilter();
		preAuthenticationFilter.setPrincipalRequestHeader("Authorization");
		preAuthenticationFilter.setCredentialsRequestHeader("Authorization");
		preAuthenticationFilter.setAuthenticationManager(authenticationManager());
		preAuthenticationFilter.setExceptionIfHeaderMissing(false);

		return preAuthenticationFilter;
	}

	@Override
	protected AuthenticationManager authenticationManager() {
		return new ProviderManager(Collections.singletonList(authenticationProvider()));
	}

	@Bean
	public AuthenticationProvider authenticationProvider() {
		PreAuthenticatedAuthenticationProvider authenticationProvider = new PreAuthenticatedAuthenticationProvider();
		authenticationProvider.setPreAuthenticatedUserDetailsService(authorizationUserDetailsService);
		return authenticationProvider;
	}

	@Bean
	public AuthenticationEntryPoint authenticationEntryPoint() {
		return new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED);
	}
}
