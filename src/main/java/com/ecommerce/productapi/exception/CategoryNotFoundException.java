package com.ecommerce.productapi.exception;

public class CategoryNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1012648693322125157L;

	public CategoryNotFoundException(Long id) {
		super("Couldn't find category " + id);
	}
}
