package com.ecommerce.productapi.exception;

public class ProductNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -6735376225091567875L;

	public ProductNotFoundException(Long id) {
		super("Couldn't find product " + id);
	}
}
