package com.ecommerce.productapi.exception;

public class CurrencyConversionException extends RuntimeException {

	private static final long serialVersionUID = -8910688014016015080L;

	public CurrencyConversionException(String msg) {
		super(msg);
	}
}
