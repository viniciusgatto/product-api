package com.ecommerce.productapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.ecommerce.productapi.model.Category;
import com.ecommerce.productapi.model.Product;

public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {

	@Query("SELECT p FROM Product p JOIN p.categories c WHERE c.id = :categoryId")
	List<Product> getProductsByCategoryId(@Param("categoryId") Long categoryId);

	@Query("SELECT p.categories FROM Product p WHERE p.id = :productId")
	List<Category> getCategoriesByProductId(@Param("productId") Long productId);
}
