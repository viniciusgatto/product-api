package com.ecommerce.productapi.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ecommerce.productapi.model.Category;

public interface CategoryRepository extends PagingAndSortingRepository<Category, Long> {
	
}