package com.ecommerce.productapi.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ecommerce.productapi.dto.CategoryDTO;
import com.ecommerce.productapi.model.Category;
import com.ecommerce.productapi.model.Product;
import com.ecommerce.productapi.service.CategoryService;

@RestController
@RequestMapping("categories")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;
	
	@GetMapping(produces = "application/json")
	public ResponseEntity<List<Category>> listAllCategories() {
		return new ResponseEntity<>(categoryService.listAllCategories(), HttpStatus.OK);
	}
	
	@GetMapping(value = "{id}", produces = "application/json")
	public ResponseEntity<Category> getCategoryById(@PathVariable Long id) {
		return new ResponseEntity<>(categoryService.getCategoryById(id), HttpStatus.OK);
	}
	
	@PostMapping(consumes = "application/json")
	public ResponseEntity<Category> addCategory(@RequestBody @Valid CategoryDTO categoryDTO) {
		Category category = categoryService.addCategory(categoryDTO);
		
		String uri = ServletUriComponentsBuilder
			.fromCurrentServletMapping()
			.path("/categories/{id}")
			.buildAndExpand(category.getId())
			.toString();
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", uri);
		
		return new ResponseEntity<>(category, headers, HttpStatus.CREATED);
	}
	
	@GetMapping(value = "{id}/products", produces = "application/json")
	public ResponseEntity<List<Product>> getProductsByCategoryId(@PathVariable Long id) {
		return new ResponseEntity<>(categoryService.getProductsByCategoryId(id), HttpStatus.OK);
	}
}