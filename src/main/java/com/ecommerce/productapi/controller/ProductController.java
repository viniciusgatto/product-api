package com.ecommerce.productapi.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ecommerce.productapi.dto.ProductDTO;
import com.ecommerce.productapi.model.Category;
import com.ecommerce.productapi.model.Product;
import com.ecommerce.productapi.service.ProductService;

@RestController
@RequestMapping("products")
public class ProductController {

	@Autowired
	private ProductService productService;

	@GetMapping(produces = "application/json")
	public ResponseEntity<List<Product>> listAllProducts() {
		return new ResponseEntity<>(productService.listAllProducts(), HttpStatus.OK);
	}
	
	@GetMapping(value = "{productId}", produces = "application/json")
	public ResponseEntity<Product> getProductById(@PathVariable Long productId) {
		return new ResponseEntity<>(productService.getProductById(productId), HttpStatus.OK);
	}

	@PostMapping(consumes = "application/json", produces = "application/json")
	public ResponseEntity<Product> addProduct(@RequestBody @Valid ProductDTO productDTO) {
		Product product = productService.addProduct(productDTO);
		
		String uri = ServletUriComponentsBuilder
			.fromCurrentServletMapping()
			.path("/products/{id}")
			.buildAndExpand(product.getId())
			.toString();
		HttpHeaders headers = new HttpHeaders();
		headers.add("Location", uri);
		
		return new ResponseEntity<>(product, headers, HttpStatus.CREATED);
	}
	
	@GetMapping(value = "{productId}/categories", produces = "application/json")
	public ResponseEntity<List<Category>> getCategoriesByProductId(@PathVariable Long productId) {
		return new ResponseEntity<>(productService.getCategoriesByProductId(productId), HttpStatus.OK);
	}
	
	@PostMapping(value = "{productId}/categories", consumes = "application/json")
	public ResponseEntity<Product> addCategoriesToProduct(@PathVariable Long productId, @RequestBody List<Long> categoryIds) {
		return new ResponseEntity<>(productService.addCategoriesToProduct(productId, categoryIds), HttpStatus.OK);
	}
}
