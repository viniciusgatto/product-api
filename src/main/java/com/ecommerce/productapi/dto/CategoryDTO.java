package com.ecommerce.productapi.dto;

import javax.validation.constraints.NotNull;

public class CategoryDTO {

	@NotNull
	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
