package com.ecommerce.productapi.dto;

import java.math.BigDecimal;
import java.util.Currency;

import javax.validation.constraints.NotNull;

public class ProductDTO {

	@NotNull
	private String description;
	@NotNull
	private BigDecimal price;
	private Currency currency;
	
	public ProductDTO() { }
	
	public ProductDTO(String description, BigDecimal price, Currency currency) {
		this.description = description;
		this.price = price;
		this.currency = currency;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
}
