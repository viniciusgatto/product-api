package com.ecommerce.productapi.security;

import java.util.Collections;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;

import com.ecommerce.productapi.exception.AuthorizationHeaderException;

@Service
public class AuthorizationUserDetailsService implements AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {

	private static final String API_KEY = "12345";
	
    @Override
    public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken token) throws UsernameNotFoundException {
    	String authorizationHeader = token.getCredentials().toString();
    	
    	if(!authorizationHeader.equals(API_KEY)) {
    		throw new AuthorizationHeaderException("Invalid Authentication");
    	}

        return loadUserDetails(authorizationHeader);
    }

    private UserDetails loadUserDetails(String apiKey) {
        return new User("user", apiKey, Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER")));
    }
}