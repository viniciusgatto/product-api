package com.ecommerce.productapi.service;

import java.math.BigDecimal;
import java.util.Currency;

public interface CurrencyService {

	BigDecimal convertAmount(Currency from, Currency to, BigDecimal amount);
}
