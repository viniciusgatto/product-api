package com.ecommerce.productapi.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ecommerce.productapi.dto.ProductDTO;
import com.ecommerce.productapi.exception.ProductNotFoundException;
import com.ecommerce.productapi.model.Category;
import com.ecommerce.productapi.model.Product;
import com.ecommerce.productapi.repository.ProductRepository;
import com.ecommerce.productapi.service.CurrencyService;
import com.ecommerce.productapi.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Value("${api.default.currency}")
	private String defaultCurrencyStr;
	
	@Autowired
	private CurrencyService currencyService;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public Product addProduct(ProductDTO productDTO) {
		Currency defaultCurrency = Currency.getInstance(defaultCurrencyStr);
		if(productDTO.getCurrency() != null && !defaultCurrency.equals(productDTO.getCurrency())) {
			BigDecimal amount = currencyService.convertAmount(productDTO.getCurrency(), defaultCurrency, productDTO.getPrice());
			productDTO.setPrice(amount);
		}
		Product product = new Product(productDTO);
		return productRepository.save(product);
	}
	
	@Override
	public Product getProductById(Long id) {
		Optional<Product> optional = productRepository.findById(id);
		if(!optional.isPresent()) {
			throw new ProductNotFoundException(id);
		}
		return optional.get();
	}
	
	@Override
	public List<Product> listAllProducts() {
		return (List<Product>) productRepository.findAll();
	}

	@Override
	public List<Category> getCategoriesByProductId(Long productId) {
		Optional<Product> optional = productRepository.findById(productId);
		if(!optional.isPresent()) {
			throw new ProductNotFoundException(productId);
		}
		return new ArrayList<>(optional.get().getCategories());
	}

	@Override
	public Product addCategoriesToProduct(Long productId, List<Long> categoryIds) {
		Optional<Product> optional = productRepository.findById(productId);
		if(!optional.isPresent()) {
			throw new ProductNotFoundException(productId);
		}
		Product product = optional.get();
		product.setCategories(createCategoriesSet(categoryIds));
		return productRepository.save(product);
	}
	
	private Set<Category> createCategoriesSet(List<Long> categoryIds) {
		Set<Category> categories = new HashSet<>();
		categoryIds.forEach(id -> {
			Category category = new Category();
			category.setId(id);
			categories.add(category);
		});
		return categories;
	}
}
