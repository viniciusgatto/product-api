package com.ecommerce.productapi.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecommerce.productapi.dto.CategoryDTO;
import com.ecommerce.productapi.exception.CategoryNotFoundException;
import com.ecommerce.productapi.model.Category;
import com.ecommerce.productapi.model.Product;
import com.ecommerce.productapi.repository.CategoryRepository;
import com.ecommerce.productapi.repository.ProductRepository;
import com.ecommerce.productapi.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public List<Category> listAllCategories() {
		return (List<Category>) categoryRepository.findAll();
	}
	
	@Override
	public Category getCategoryById(Long id) {
		Optional<Category> optional = categoryRepository.findById(id);
		if(!optional.isPresent()) {
			throw new CategoryNotFoundException(id);
		}
		return optional.get();
	}
	
	@Override
	public Category addCategory(CategoryDTO categoryDTO) {
		Category category = new Category(categoryDTO);
		return categoryRepository.save(category);
	}

	@Override
	public List<Product> getProductsByCategoryId(Long categoryId) {
		return productRepository.getProductsByCategoryId(categoryId);
	}	
}
