package com.ecommerce.productapi.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.ecommerce.productapi.exception.CurrencyConversionException;
import com.ecommerce.productapi.service.CurrencyService;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

@Service
public class CurrencyServiceImpl implements CurrencyService {

	@Value("${currency.converter.endpoint}")
	private String currencyRateServiceEnpoint;
	
	@Value("${currency.converter.apiKey}")
	private String currencyRateServiceApiKey;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	public BigDecimal convertAmount(Currency from, Currency to, BigDecimal amount) {
		
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(currencyRateServiceEnpoint)
			.queryParam("access_key", currencyRateServiceApiKey)
			.queryParam("base", to.getCurrencyCode())
			.queryParam("symbols", from.getCurrencyCode());
		
		ResponseEntity<String> response = restTemplate.getForEntity(uriBuilder.build().toUri(), String.class);
		
		if(!response.getStatusCode().is2xxSuccessful()) {
			throw new CurrencyConversionException("Currency Service HttpStatus = " + response.getStatusCodeValue());
		}
		
		DocumentContext ctx = JsonPath.parse(response.getBody());
		BigDecimal rate = ctx.read("$.rates." + from.getCurrencyCode(), BigDecimal.class);
		return amount.divide(rate, 2, RoundingMode.HALF_UP);
	}

}
