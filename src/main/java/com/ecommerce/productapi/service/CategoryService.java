package com.ecommerce.productapi.service;

import java.util.List;

import com.ecommerce.productapi.dto.CategoryDTO;
import com.ecommerce.productapi.model.Category;
import com.ecommerce.productapi.model.Product;

public interface CategoryService {

	Category addCategory(CategoryDTO category);
	Category getCategoryById(Long id);
	List<Category> listAllCategories();
	List<Product> getProductsByCategoryId(Long id);
}
