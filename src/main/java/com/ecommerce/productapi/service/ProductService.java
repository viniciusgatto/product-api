package com.ecommerce.productapi.service;

import java.util.List;

import com.ecommerce.productapi.dto.ProductDTO;
import com.ecommerce.productapi.model.Category;
import com.ecommerce.productapi.model.Product;

public interface ProductService {

	Product addProduct(ProductDTO productDTO);
	List<Product> listAllProducts();
	Product getProductById(Long id);
	List<Category> getCategoriesByProductId(Long id);
	Product addCategoriesToProduct(Long productId, List<Long> categoryIds);
}
