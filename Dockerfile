FROM openjdk:8-jre-alpine

COPY ./target/product-api-*.jar /opt/product-api.jar

WORKDIR /opt

EXPOSE 8080

CMD java -jar product-api.jar